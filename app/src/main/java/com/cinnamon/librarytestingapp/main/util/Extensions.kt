package com.cinnamon.librarytestingapp.main.util

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.cinnamon.librarytestingapp.main.util.APP_KEY
import com.cinnamon.librarytestingapp.main.util.APP_SECRET
import com.cinnamon.librarytestingapp.main.util.ENVIRONMENT
import com.cinnamon.librarytestingapp.main.util.PERMISSIONS_REQUEST_CODE
import com.google.android.material.snackbar.Snackbar
import com.sinch.android.rtc.Sinch
import com.sinch.android.rtc.SinchClient

fun snack(view: View, msg: String) {
    Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show()
}

fun Activity.askPermissions(permissions: Array<String>) {
    ActivityCompat.requestPermissions(this, permissions, PERMISSIONS_REQUEST_CODE)
}

fun Context.checkPermission(permission: String): Boolean =
    ContextCompat.checkSelfPermission(
        this,
        permission
    ) == PackageManager.PERMISSION_GRANTED

fun Context.getSinchClient(callerId: String): SinchClient = Sinch.getSinchClientBuilder()
    .context(this)
    .userId(callerId)
    .applicationKey(APP_KEY)
    .applicationSecret(APP_SECRET)
    .environmentHost(ENVIRONMENT)
    .build()