package com.cinnamon.librarytestingapp.main.services

import android.content.Intent
import android.os.Bundle
import android.os.IBinder
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import androidx.media.MediaBrowserServiceCompat
import androidx.media.session.MediaButtonReceiver
import timber.log.Timber

class MediaPlaybackService : MediaBrowserServiceCompat() {

    private val mediaSessionCallback = object : MediaSessionCompat.Callback() {
        override fun onMediaButtonEvent(mediaButtonIntent: Intent): Boolean {
            Timber.tag("MEDIA_BUTTON_EVENT").d("CLICKED <-> MBI")
            return super.onMediaButtonEvent(mediaButtonIntent)
        }

        override fun onCustomAction(action: String?, extras: Bundle?) {
            Timber.tag("MEDIA_BUTTON_EVENT").d("CLICKED <-> MBI")
            super.onCustomAction(action, extras)
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onGetRoot(
        clientPackageName: String,
        clientUid: Int,
        rootHints: Bundle?
    ): BrowserRoot? {
        Timber.d("onGetRoot -> IN")
        return null
    }

    override fun onLoadChildren(
        parentId: String,
        result: Result<MutableList<MediaBrowserCompat.MediaItem>>
    ) {
        Timber.d("onLoadChildren -> LOADED")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val mediaSession = MediaSessionCompat(applicationContext, "MEDIA_SESSION_COMPAT")
        val state =
            PlaybackStateCompat.Builder().setActions(PlaybackStateCompat.ACTION_PLAY).build()
        Timber.d("SERVICE -> $intent")
        mediaSession.setPlaybackState(state)
        mediaSession.setCallback(mediaSessionCallback)
        mediaSession.isActive = true
        MediaButtonReceiver.handleIntent(mediaSession, intent)
        return START_STICKY
    }
}