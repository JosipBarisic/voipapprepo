package com.cinnamon.librarytestingapp.main.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.sinch.android.rtc.calling.Call

class MainViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    val userId = MutableLiveData<String>()
    val recipientId = MutableLiveData<String>()
    val call = MutableLiveData<Call>()

    val setupCheck = MutableLiveData(false)
    val callCheck = MutableLiveData(false)
    val answerCheck = MutableLiveData(false)
    val hangupCheck = MutableLiveData(false)
    val showCallScreen = MutableLiveData(false)
    val showAnswerBtn = MutableLiveData(false)

    fun setupClient() {
        setupCheck.value = true
    }

    fun call() {
        callCheck.value = true
    }

    fun answer() {
        answerCheck.value = true
    }

    fun hangup() {
        hangupCheck.value = true
        showCallScreen.value = false
    }
}