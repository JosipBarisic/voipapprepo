package com.cinnamon.librarytestingapp.main.view

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.*
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.session.PlaybackState
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.view.KeyEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.cinnamon.librarytestingapp.R
import com.cinnamon.librarytestingapp.databinding.ActivityMainBinding
import com.cinnamon.librarytestingapp.main.services.MediaPlaybackService
import com.cinnamon.librarytestingapp.main.util.*
import com.cinnamon.librarytestingapp.main.viewmodel.MainViewModel
import com.sinch.android.rtc.*
import com.sinch.android.rtc.calling.Call
import com.sinch.android.rtc.calling.CallClient
import com.sinch.android.rtc.calling.CallClientListener
import com.sinch.android.rtc.calling.CallListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.call_screen.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var dataBinding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()

    // Get the default adapter
    private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    var bluetoothHeadset: BluetoothHeadset? = null
    var bluetoothSocket: BluetoothSocket? = null

    //Media session
//    private lateinit var mediaSession: MediaSessionCompat
    private lateinit var mediaIntent: Intent

    private lateinit var audioManager: AudioManager
    private lateinit var inputMethodManager: InputMethodManager

    private lateinit var sinchClient: SinchClient
    private lateinit var callClient: CallClient
    private lateinit var call: Call

    private lateinit var player: MediaPlayer
    private var timer: Timer? = null
    private var timePassed: Long = 0

    private val sinchClientListener = object : SinchClientListener {
        override fun onClientStarted(sc: SinchClient?) {
            sc?.let {
                callClient = it.callClient
                callClient.addCallClientListener(callClientListener)
                snack(setup_btn, "Client started")
            }
        }

        override fun onClientStopped(p0: SinchClient?) {
            Timber.tag("SINCH_CLIENT").d("stopped")
        }

        override fun onClientFailed(p0: SinchClient?, p1: SinchError?) {
            Timber.tag("SINCH_CLIENT").d("failed")
        }

        override fun onRegistrationCredentialsRequired(p0: SinchClient?, p1: ClientRegistration?) {
            Timber.tag("SINCH_CLIENT").d("registration required")
        }

        override fun onLogMessage(p0: Int, p1: String?, p2: String?) {
            Timber.tag("SINCH_CLIENT").d("log msg $p0 $p1 $p2")
        }
    }

    private val callClientListener =
        CallClientListener { _, c ->
            call = c
            snack(call_btn, "INCOMING CALL")
            playMusic()
            call.addCallListener(callListener)
            mainViewModel.showCallScreen.value = true
            mainViewModel.showAnswerBtn.value = true
            GlobalScope.launch(Dispatchers.Main) {
                user_calling.text = String.format(call.remoteUserId, R.string.talking_to_user)
            }
        }

    private val answerCallObserver = Observer<Boolean> {
        if (it) {
            Timber.tag("CALL_FLOW_ANSWER").d("ANSWERING ${call.details}")
            call.answer()
            mainViewModel.answerCheck.value = false
        }
    }

    private val hangupObserver = Observer<Boolean> {
        if (it) {
            Timber.tag("CALL_FLOW_HANGUP").d("HANGUP ${call.details}")
            GlobalScope.launch(Dispatchers.Main) {
                call_time.text = ""
            }
            call.hangup()
            mainViewModel.hangupCheck.value = false
        }
    }

    private val callListener = object : CallListener {
        override fun onCallProgressing(call: Call?) {
            Timber.tag("CALL_FLOW").d("PROGRESSING ${call?.details}")
//            audioManager.mode = AudioManager.MODE_RINGTONE
        }

        @RequiresApi(Build.VERSION_CODES.O)
        override fun onCallEstablished(call: Call?) {
            Timber.tag("CALL_FLOW").d("ESTABLISHED ${call!!.details}")
            stopMusic()

            mainViewModel.showAnswerBtn.value = false

            GlobalScope.launch(Dispatchers.Main) {
                user_calling.text = String.format(call.remoteUserId, R.string.talking_to_user)
            }
            /*volumeControlStream = AudioManager.STREAM_VOICE_CALL
            audioManager.mode = AudioManager.MODE_IN_CALL*/

            bluetoothHeadset?.let {
                audioManager.startBluetoothSco()
                audioManager.isBluetoothScoOn = true
                Timber.tag("CALL_FLOW_BT_NAME").d(it.connectedDevices[0].name)
                Timber.tag("CALL_FLOW_BT_TYPE").d(it.connectedDevices[0].type.toString())
                Timber.tag("CALL_FLOW_BT_CLASS").d(it.connectedDevices[0].bluetoothClass.toString())
                Timber.tag("CALL_FLOW_BT_ALL").d(it.connectedDevices.toString())

                for (device in it.connectedDevices) {
                    Timber.tag("CALL_DEVICE_AC").d(device.name)
                    Timber.tag("CALL_DEVICE_AC").d(device.uuids.first().uuid.toString())
                    Timber.tag("CALL_DEVICE_AC").d(device.address)
                    Timber.tag("CALL_DEVICE_AC").d(device.bluetoothClass.toString())
                    Timber.tag("CALL_DEVICE_AC")
                        .d(bluetoothHeadset?.getConnectionState(device).toString())
                    bluetoothHeadset?.startVoiceRecognition(device)
                    if (device.bondState == BluetoothDevice.BOND_BONDED)
                        Timber.tag("CALL_DEVICE_CONNECTED").d("device -> ${device.name}")
                }
                Timber.tag("CALL_BT_HEADSET").d("$bluetoothHeadset <-<")
            }
        }

        override fun onCallEnded(call: Call?) {
            Timber.tag("CALL_FLOW").d("ENDED BECAUSE -> ${call?.details?.endCause}")
            stopMusic()
            mainViewModel.showCallScreen.value = false
            volumeControlStream = AudioManager.USE_DEFAULT_STREAM_TYPE
            GlobalScope.launch(Dispatchers.Main) {
                user_calling.text = ""
            }
            /*audioManager.mode = AudioManager.MODE_NORMAL
            volumeControlStream = AudioManager.USE_DEFAULT_STREAM_TYPE*/
        }

        override fun onShouldSendPushNotification(call: Call?, p1: MutableList<PushPair>?) {
            Timber.tag("CALL_FLOW").d("NOTIFICATION")
        }
    }

    private val setupClientObserver = Observer<Boolean> { setup ->
        if (setup) {
            if (checkPermission(Manifest.permission.RECORD_AUDIO)
                && checkPermission(Manifest.permission.CALL_PHONE)
                && checkPermission(Manifest.permission.READ_PHONE_STATE)
            ) {
                mainViewModel.userId.value?.let { userId ->
                    Timber.tag("USER_ID").d(userId)
                    setUpSinchClient(userId)
                }

            } else askPermissions(
                arrayOf(
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.READ_PHONE_STATE
                )
            )
            mainViewModel.setupCheck.value = false
            inputMethodManager.hideSoftInputFromWindow(dataBinding.root.windowToken, 0)
        }
    }

    private val callCheckObserver = Observer<Boolean> {
        if (it) {
            mainViewModel.recipientId.value?.let { recipientId ->
                Timber.tag("CALL_FLOW_CALLING").d(recipientId)
                call = callClient.callUser(recipientId)
                call.addCallListener(callListener)
                mainViewModel.showCallScreen.value = true
                mainViewModel.showAnswerBtn.value = false
                inputMethodManager.hideSoftInputFromWindow(dataBinding.root.windowToken, 0)
            }
        }
    }

    private val profileListener = object : BluetoothProfile.ServiceListener {

        override fun onServiceConnected(profile: Int, proxy: BluetoothProfile) {
            if (profile == BluetoothProfile.HEADSET) {
                bluetoothHeadset = proxy as BluetoothHeadset
            }
        }

        override fun onServiceDisconnected(profile: Int) {
            if (profile == BluetoothProfile.HEADSET) {
                bluetoothHeadset = null
            }
        }
    }

    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {
        Timber.tag("CALL_KEY_EVENT").d("$event")
        return super.dispatchKeyEvent(event)
    }

    private val mediaSessionCallback = object : MediaSessionCompat.Callback() {
        override fun onMediaButtonEvent(mediaButtonIntent: Intent): Boolean {
            Toast.makeText(applicationContext, "Media Button", Toast.LENGTH_LONG).show()
            return super.onMediaButtonEvent(mediaButtonIntent)
        }

        override fun onCustomAction(action: String?, extras: Bundle?) {
            Timber.tag("MEDIA_BUTTON_EVENT").d("CLICKED <-> MBI")
            Toast.makeText(applicationContext, "Media Button", Toast.LENGTH_LONG).show()
            super.onCustomAction(action, extras)
        }
    }

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        dataBinding.mainViewModel = mainViewModel
        dataBinding.lifecycleOwner = this

        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        player = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI)

        mediaIntent = Intent(this, MediaPlaybackService::class.java)
        startService(mediaIntent)

        /*val mediaSession = MediaSessionCompat(this, "MEDIA_SESSION_COMPAT")
        val state = PlaybackStateCompat.Builder().setActions(PlaybackState.ACTION_PLAY)
            .build()

        mediaSession.setPlaybackState(state)
        mediaSession.setCallback(mediaSessionCallback)
        mediaSession.isActive = true*/

        val btIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        enable_bt_btn.setOnClickListener {
            if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled)
                startActivityForResult(btIntent, BT_REQUEST_CODE)
            else {
                snack(enable_bt_btn, "Bluetooth already enabled!")
                bluetoothAdapter.getProfileProxy(this, profileListener, BluetoothProfile.HEADSET)

                val devices = bluetoothAdapter.bondedDevices
                for (device in devices) {
                    Timber.tag("DEVICE_").d("--------------------")
                    Timber.tag("DEVICE_NAMES").d(device.name)
                    Timber.tag("DEVICE_CLASS").d(device.bluetoothClass.deviceClass.toString())
                    Timber.tag("DEVICE_ADDR").d(device.address)
                    Timber.tag("DEVICE_uuid").d(device.uuids[0].uuid.toString())
                    Timber.tag("DEVICE_BOND_STATE").d(device.bondState.toString())
                    Timber.tag("DEVICE_").d("--------------------")
                }
            }
        }

        mainViewModel.answerCheck.observe(this, answerCallObserver)
        mainViewModel.hangupCheck.observe(this, hangupObserver)

        mainViewModel.setupCheck.observe(this, setupClientObserver)
        mainViewModel.callCheck.observe(this, callCheckObserver)
    }

    override fun onDestroy() {
        super.onDestroy()
        bluetoothAdapter?.closeProfileProxy(BluetoothProfile.HEADSET, bluetoothHeadset)
        stopService(mediaIntent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == BT_REQUEST_CODE && resultCode == RESULT_OK) {
            snack(enable_bt_btn, "Bluetooth working")
            bluetoothAdapter?.getProfileProxy(this, profileListener, BluetoothProfile.HEADSET)

            val devices = bluetoothAdapter.bondedDevices
            for (device in devices) {
                Timber.tag("DEVICE_").d("--------------------")
                Timber.tag("DEVICE_NAMES").d(device.name)
                Timber.tag("DEVICE_CLASS").d(device.bluetoothClass.deviceClass.toString())
                Timber.tag("DEVICE_ADDR").d(device.address)
                Timber.tag("DEVICE_uuid").d(device.uuids[0].uuid.toString())
                Timber.tag("DEVICE_BOND_STATE").d(device.bondState.toString())
                Timber.tag("DEVICE_").d("--------------------")
            }

            bluetoothHeadset?.let {
                Timber.d("HEADSET CONNECTED")
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) Timber.d("PERMISSIONS GRANTED")
    }

    private fun setUpSinchClient(callerId: String) {
        sinchClient = getSinchClient(callerId)
        sinchClient.addSinchClientListener(sinchClientListener)
        sinchClient.setSupportCalling(true)
        sinchClient.setSupportActiveConnectionInBackground(true)
        sinchClient.startListeningOnActiveConnection()
        sinchClient.start()
    }

    private fun playMusic() {
        if (!player.isPlaying) player.start()
    }

    private fun stopMusic() {
        if (player.isPlaying) player.stop()
    }
}